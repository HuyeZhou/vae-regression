# vae-regression

Repo for Project: Applying vae-regression model on patient mutation data to extract target related latent variables and predict overall survival

# Code and Data Directory Structure
Code and data directories should have a short, descriptive, name and follow a consistent template:

    vae-regression/
        - /src:
            - Contains function for building model, data processing,  results analysis .etc
        - /temp:
            - Contains any intermediate files produced by code
        - /data:
            - Raw data storage for directories consuming raw data as input.
        - /output:
            - Any data or plots produced as final output by code in /src. Other directories can consume the contents of this folder.