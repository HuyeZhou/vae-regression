# get raw data
# python src/get_mutations.py
# python src/get_survival.py

# preprocess
# python src/merge_raw_cbsp.py old
# python src/process_raw_MTB.py old
# python src/process_raw_ipredict.py old
# python src/merge_raw_cbsp.py new
# python src/process_raw_MTB.py new
# python src/process_raw_ipredict.py new
# python src/create_tfrecord.py cbsp
# python src/create_tfrecord.py cbsp_MTB
# python src/create_tfrecord.py cbsp_MTB_new


# train
# python src/train.py cbsp 21588 100 
# python src/train.py cbsp 3000 100 
# python src/train.py cbsp 1000 100 
# python src/train.py cbsp_MTB 358 100 
# python src/train.py cbsp_MTB_new 367 100 
# python src/train.py MTB_IP 358 100 
# python src/train.py MTB_PFS 358 100 


# python src/test.py cbsp train cbsp 21588 100 1e-5 1e-8 100 128 16 0.25 3
# python src/test.py cbsp test cbsp 21588 100 1e-5 1e-8 100 128 16 0.25 3
# python src/test.py cbsp train cbsp 1000 100 1e-5 1e-8 100 128 16 0.5 3
# python src/test.py cbsp test cbsp 1000 100 1e-5 1e-8 100 128 16 0.5 3
# python src/test.py cbsp_MTB train cbsp_MTB 358 100 1e-5 1e-7 50 128 16 0.25 3
# python src/test.py cbsp_MTB test cbsp_MTB 358 100 1e-5 1e-7 50 128 16 0.25 3
# python src/test.py MTB test cbsp_MTB 358 100 1e-5 1e-7 50 128 16 0.25 3
# python src/test.py ipredict test cbsp_MTB 358 100 1e-5 1e-7 50 128 16 0.25 3
# python src/test.py MTB_ipredict test cbsp_MTB 358 100 1e-5 1e-7 50 128 16 0.25 3
# python src/test.py cbsp_MTB train cbsp_MTB 358 100 1e-4 1e-6 100 128 16 0.25 3
# python src/test.py cbsp_MTB test cbsp_MTB 358 100 1e-4 1e-6 100 128 16 0.25 3
# python src/test.py MTB test cbsp_MTB 358 100 1e-4 1e-6 100 128 16 0.25 3
# python src/test.py ipredict test cbsp_MTB 358 100 1e-4 1e-6 100 128 16 0.25 3
python src/test.py cbsp_MTB_new test cbsp_MTB_new 367 100 1e-4 1e-8 100 128 16 0.25 2
python src/test.py MTB_new test cbsp_MTB_new 367 100 1e-4 1e-8 100 128 16 0.25 3
python src/test.py ipredict_new test cbsp_MTB_new 367 100 1e-4 1e-8 100 128 16 0.25 3
python src/test.py ipredict test MTB_IP 358 100 1e-4 1e-7 50 128 32 0.5 2