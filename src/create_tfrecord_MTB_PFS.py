import os
import sys
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
os.environ['TF_GPU_THREAD_MODE'] = 'gpu_private'
import tensorflow as tf
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler

devices_id = 3
physical_devices = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[devices_id], True)
tf.config.set_visible_devices(physical_devices[devices_id], 'GPU')
np.random.seed(1234)
tf.random.set_seed(1234)


def main():
    df_MTB = pd.read_csv("data/interim/MTB_PFS_processed.csv")
    df_MTB = df_MTB.dropna()
    df_MTB['OS_MONTHS'] = np.log(df_MTB['OS_MONTHS'])
    scaler = StandardScaler()
    scaler.fit(df_MTB[['OS_MONTHS']])
    df_MTB['OS_MONTHS'] = scaler.transform(df_MTB[['OS_MONTHS']])
    MTB_ds = tf.data.Dataset.from_tensor_slices(dict(df_MTB))
    write_example(MTB_ds, 'MTB_PFS', 'train')
    df_IP = pd.read_csv("data/interim/ipredict_PFS_processed.csv")
    df_IP = df_IP.dropna()
    df_IP['OS_MONTHS'] = np.log(df_IP['OS_MONTHS'])
    df_IP['OS_MONTHS'] = scaler.transform(df_IP[['OS_MONTHS']])
    IP_ds = tf.data.Dataset.from_tensor_slices(dict(df_IP))
    write_example(IP_ds, 'MTB_PFS', 'test')
    
    print("Complete!")

def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):
        value = value.numpy()  # BytesList won't unpack a string from an EagerTensor.
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
    """Returns a float_list from a float / double."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def serialize_example(row):
    feature = {
        'id': _bytes_feature(row['id']),
        'os_months': _float_feature(tf.cast(row['OS_MONTHS'], tf.float32)),
        'os_status': _int64_feature(row['OS_STATUS']),
        'cancer_type': _int64_feature(row['cancerTypeId']),
        'gene': _bytes_feature(row['gene'])
    }
    example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
    return example_proto.SerializeToString()


def write_example(dataset, name, subset):
    print("Start creating {} set...".format(subset))
    with tf.io.TFRecordWriter('data/processed/{}_{}.tfrecords'.format(name, subset)) as writer:
        nr = 0
        for row in dataset:
            if (nr % 10000) == 0:
                print("On row: {}".format(nr))
            nr += 1
            example = serialize_example(row)
            writer.write(example)
        print("Finish! Adding {} samples in {} set".format(nr, subset))


if __name__ == "__main__":
    main()
