import pandas as pd
import numpy as np
import sys

which = sys.argv[1]

def main():
    MTB_df = pd.read_excel("data/raw/preprocessed_MTB_data.xlsx", engine='openpyxl')
    MTB_df = MTB_df.iloc[:, [0,3,14,15]]
    if which == "new":
        MTB_mutations = pd.read_csv("data/raw/one_hot_MTB_data_new.csv").iloc[:, 176:543]
    else:
        MTB_mutations = pd.read_csv("data/raw/MTB_mutations.csv")
    MTB_df['gene_idx'] = MTB_mutations.astype(bool).apply(func,axis=1)
    MTB_df['gene'] = [','.join(map(str, i)) for i in MTB_df["gene_idx"]]
    MTB_df = MTB_df.drop(columns=["gene_idx"]).rename(columns={"Study.ID": "id", 
                                                               "Diagnosis": "cancerTypeId", 
                                                               "OS_Months": "OS_MONTHS", 
                                                               "OS_Censoring_Status": "OS_STATUS"
                                                               })
    if which == "new":
        MTB_df.to_csv('data/interim/MTB_new_processed.csv', header=True, index=False, mode='w')
    else:
        MTB_df.to_csv('data/interim/MTB_processed.csv', header=True, index=False, mode='w')


def func(row):
    if which == "new":
        ints = np.array(range(0, 367))[row].tolist()
    else:
        ints = np.array(range(0, 358))[row].tolist()
    return([str(int) for int in ints])

if __name__ == "__main__":
    main()
