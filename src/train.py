import sys
from utils import *

devices_id = int(sys.argv[4])
gpus = tf.config.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        tf.config.experimental.set_visible_devices(gpus[devices_id], 'GPU')
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
    except RuntimeError as e:
        print(e)

np.random.seed(1234)
tf.random.set_seed(1234)

bs = 16
HP_LR = hp.HParam('lr', hp.Discrete([1e-4, 1e-5]))
HP_L2 = hp.HParam('l2', hp.Discrete([1e-6, 1e-7, 1e-8]))
HP_DS = hp.HParam('ds', hp.Discrete([50, 100, 200]))
HP_NF = hp.HParam('nf', hp.Discrete([4, 8, 16]))
HP_LD = hp.HParam('ld', hp.Discrete([1,2]))
HP_DR = hp.HParam('dr', hp.Discrete([0.25, 0.5]))

ds_name = sys.argv[1]
input_dim = int(sys.argv[2])  # max(input_dim) = 21588 for cbsp, input_dim = 358 for cbsp_MTB, , input_dim = 367 for cbsp_MTB_new
epochs = int(sys.argv[3])

out_dir = 'output/models/VAE_REG_{}_{}_{}'.format(ds_name, input_dim, epochs)
logdir = 'output/models/VAE_REG_{}_{}_{}/logs'.format(ds_name, input_dim, epochs)
checkdir = 'output/models/VAE_REG_{}_{}_{}/checkpoints'.format(ds_name, input_dim, epochs)


def main():
    train_ds = get_ds(ds_name, "train", input_dim, bs, shuffle=True)
    test_ds = get_ds(ds_name, "test", input_dim, bs, shuffle=False)
    test_label = np.concatenate([y for _, y in test_ds], axis=0)
    print("Start finetuning of model for {} with epochs={}".format(ds_name, epochs))
    print("You can use tensorboard to monitor the process $ tensorboard --logdir='{}'".format(logdir))
    session_num = 0
    combined = [(lr, l2, ds, nf, ld, dr)
                for lr in HP_LR.domain.values
                for l2 in HP_L2.domain.values
                for ds in HP_DS.domain.values
                for nf in HP_NF.domain.values
                for ld in HP_LD.domain.values
                for dr in HP_DR.domain.values
                ]

    with tf.summary.create_file_writer(logdir + '/hparam_tuning/').as_default():
        hp.hparams_config(
            hparams=[HP_LR, HP_L2, HP_DS, HP_NF, HP_LD, HP_DR],
            metrics=[hp.Metric('test_label_loss', display_name='Test_Label_Loss'),
                     hp.Metric('test_r_square', display_name='Test_R_square')
                     ],
        )

    for (lr, l2, ds, nf, ld, dr) in combined:
        hparams = {
            'lr': lr,
            'l2': l2,
            'ds': ds,
            'nf': nf,
            'ld': ld,
            'dr': dr,
        }
        tf.keras.backend.clear_session()
        run_name = "run-%d" % session_num
        print('--- Starting trial: %s' % run_name)
        print({h: hparams[h] for h in hparams})
        vae, encoder, decoder = get_vae_model(input_dim, ld, l2, nf, dropout_rate=dr)
        run_dir = logdir + '/hparam_tuning/{}_{}_{}_{}_{}_{}'.format(lr, l2, ds, nf, ld, dr)
        run(run_dir, hparams, vae, encoder, train_ds, test_ds, test_label, logdir, checkdir, epochs)
        session_num += 1
        print('--- End trial: %s' % run_name)

    print("Complete! please use tensorboard to check the result. $ tensorboard --logdir='{}'".format(logdir))


if __name__ == "__main__":
    main()
